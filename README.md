# README #
This README would normally document whatever steps are necessary to get your application up and running.

### To minifiy ###
1) `node build --env=production`

### Dev Dependencies ###
1) Python download [ here ](https://www.python.org/downloads/release/python-2712/)
2) Node js, download [ here ](https://nodejs.org/en/)
3) Add these to the windows environment paths

### How to use this builder ###
* its quite complex at first but really its just a config file.
* Which ever project you have installed this builder to copy the builder-config file to the root of your project.

### JS WATCHERS AND CSS WATCHERS ###
* This package uses gulp you will need to know about the gulp watches and how to select which ever files you want to watch. * In the example you can see I'm watching all .html and .js in the /client/js/ folder and deep nested files.
* Same with the css, im watching all scss files in the /client/scss/ I have in my project.
* You will need to change these and add as many different selectors you will need.

### JS CONFIG RULES ###
1) If the filename of the object ends in .js then it will be built into a file and exported to the dir you set in the "export" key.
2) All other like in the example like "base" is a collection of files that can be referrenced in another object to include that set.
3) The dir is just a base dir include you have alot of files, then you don't need to write the whole path in every line
4) the concat order is the order that you files for that object will be concatenated in. But remember that you includes will be added first.

### Extra ###
* Its quite confusing at first, but I want to make something very generic and easy to use once you have a foundation.
* Let me know if there are any bugs etc. Thanks you using this!.

### Who do I talk to? ###
* Andrew Butler
