var commandParams = require('minimist')(process.argv.slice(2)),
    environment = commandParams.env == 'production' ? 'prod' : 'dev',
    path = require('path');

module.exports = {
    returnEnvrDir: function returnEnvrDir(stringDir, environment) {
        var dir = stringDir;

        switch (environment) {
            case 'prod':
                dir = path.join(dir, 'release');
                break;
            default:
                dir = path.join(dir, 'dev');
        }

        return dir;
    },

    getEnvir : function () {
        return environment;
    }
};
