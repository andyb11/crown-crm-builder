//package vars
var commandParams = require('minimist')(process.argv.slice(2)),
    jsBuilder = require('./lib/js-builder'),
    cssBuilder = require('./lib/css-builder'),
    jsWatcher = require('./lib/js-watcher'),
    cssWatcher = require('./lib/css-watcher'),
    gulp = require('gulp');

module.exports = function crownloader(config) {
    if (!config) {
        return false;
    }

    var jsFiles = {},
        cssFiles = {},
        localCssWatcher,
        localJsWatcher,
        jsBuilders = [],
        scssBuilders = [];

    //start the progam
    if (config.js) {
        prepareJs();
    }

    if (config.css) {
        prepareCss();
    }

    function prepareJs() {
        var output = config.js;

        for (var i = 0; i < output.length; i++) {
            var obj = output[i],
                dir = obj.dir;

            //loop through the config to concat the base dir with the files
            for (var x = 0; x < obj.concatOrder.length; x++) {
                obj.concatOrder[x] = dir + '/' + obj.concatOrder[x] + '.js';
            }

            if (obj.filename.split('.').pop() !== 'js') {
                continue;
            }

            //concat any include paths that are needed in the certain object
            if (obj.include && obj.include.length > 0) {

                var newIncludeArry = [];
                for (var y = 0; y < obj.include.length; y++) {
                    var includeName = obj.include[y],
                        includeFiles = findFileConfig(includeName);

                    if (!includeFiles) {
                        console.log('Error : cannot find relevant include object');
                        continue;
                    }

                    newIncludeArry = newIncludeArry.concat(includeFiles.concatOrder);
                }

                obj.concatOrder = newIncludeArry.concat(obj.concatOrder);
            }

            //start the build process
            jsBuilders.push(new jsBuilder(obj));
        }

        if (config.jsWatches && config.jsWatches.length) {
            localJsWatcher = new jsWatcher(config.jsWatches, jsBuilders);
        }



    }

    function findFileConfig(name) {
        var output = config.js,
            foundObj;

        for (var i = 0; i < output.length; i++) {
            var obj = output[i];

            if (obj.filename === name) {
                foundObj = obj;
                break;
            }
        }

        return foundObj;
    }

    function prepareCss() {
        var cssArry = config.css.files,
            baseData = config.css.default;

        for (var i = 0; i < cssArry.length; i++) {
            scssBuilders.push(new cssBuilder(cssArry[i], baseData));
        }

        localCssWatcher = new cssWatcher(config.css.watches, scssBuilders)
    }
}
