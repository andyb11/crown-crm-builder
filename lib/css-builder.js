var gulp = require('gulp'),
    concat = require('gulp-concat'),
    uglifycss = require('gulp-uglifycss'),
    sass = require('gulp-sass'),
    rename = require('gulp-rename'),
    helper = require('./helper');

function cssHandler(data) {
    var env = helper.getEnvir(),
        filename = data.start.substring(data.start.lastIndexOf('/') + 1, data.start.lastIndexOf('.'));

    switch (env) {
        case 'prod':
            filename += '.live.css';
            break;
        case 'dev':
            filename += '.dev.css';
            break;
    }

    buildCss();

    function buildCss() {
        console.log(`building - ${data.start}`);
        var map = gulp.src(data.start)
            .pipe(concat('concat.css'))
            .pipe(sass().on('error', sass.logError))
            .pipe(rename(filename));


        if(env == 'prod') {
            map.pipe(uglifycss());
        }

        map.pipe(gulp.dest(data.export));
    }

    return buildCss;
}

module.exports = cssHandler;
