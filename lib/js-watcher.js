var gulp = require('gulp');

jsWatcher = function(selectors, builders) {
    if (!selectors || !selectors.length || !builders || !builders.length)
        return;

    var viewsDir = selectors;

    for (var i = 0; i < viewsDir.length; i++) {
        var obj = viewsDir[i];

        gulp.watch([obj], jsViewsChange);
    }

    function jsViewsChange() {
        for (var i = 0; i < builders.length; i++) {
            builders[i]();
        }
    }

}

module.exports = jsWatcher;
