var gulp = require('gulp');


cssWatcher = function(selectors, builders) {
    if (!selectors || !selectors.length || !builders || !builders.length)
        return;

    var watches = selectors;

    for (var i = 0; i < watches.length; i++) {
        var watch = watches[i];

        gulp.watch(watch, cssChange);
    }

    function cssChange() {
        for (var i = 0; i < builders.length; i++) {
            builders[i]();
        }
    }

}

module.exports = cssWatcher;
