var gulp = require('gulp'),
    concat = require('gulp-concat'),
    uglify = require('gulp-uglify'),
    rename = require('gulp-rename'),
    sourcemaps = require('gulp-sourcemaps'),
    injectHtml = require('gulp-inject-stringified-html'),
    helper = require('./helper');


//start the process of building/exporting and watching the files
function jsHandler(object) {
    var dir = helper.returnEnvrDir(object.export),
        env = helper.getEnvir(),
        filerename = object.filename.replace('.js', '');

    switch (env) {
        case 'prod':
            filerename += '.live.js'
            break;
        case 'dev':
            filerename += '.dev.js'
            break;
    }

    buildJs();

    function buildJs() {
        console.log(`building - ${object.filename}`);
        var map = gulp.src(object.concatOrder)
            .pipe(concat('concat.js'))
            .pipe(injectHtml())
            .pipe(rename(filerename));



        if(env == 'prod') {
            map.pipe(uglify());
        }


        map.pipe(gulp.dest(object.export));
    }

    return buildJs;

}

module.exports = jsHandler;
